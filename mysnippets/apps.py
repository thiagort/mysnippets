from django.apps import AppConfig


class MysnippetsConfig(AppConfig):
    name = 'mysnippets'
